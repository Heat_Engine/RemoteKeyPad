package com.krida.remoteKeyPad;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import com.krida.remoteKeyPad.controller.KeyInputController;

@SpringBootTest
@AutoConfigureMockMvc
class RemoteKeyPadApplicationTests {
	
	@Autowired KeyInputController controller;
	
	@Autowired MockMvc mockMvc;

	@Test
	void contextLoads() {
		assertThat(controller).isNotNull();
	}
	
	@Test
	void checkMappings() throws Exception {
		String keyMap[] = {"a","b","c","d","e"};
		for(String a : keyMap) {
			this.mockMvc.perform(get("/hit/" + a))
			.andExpect(status().isOk())
			.andExpect(content().string(containsString(a)));
		}
	}

}
