package com.krida.remoteKeyPad.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.krida.remoteKeyPad.service.KeyHitService;

import lombok.RequiredArgsConstructor;

@RestController
public class KeyInputController {
	
	private final KeyHitService keyHitService;
	
	public KeyInputController(KeyHitService hitService) {
		this.keyHitService = hitService;
	}
	
	@GetMapping("/hit/{key}")
	public String hitKey(@PathVariable String key) {
		return keyHitService.hitKey(key);
	}
}
