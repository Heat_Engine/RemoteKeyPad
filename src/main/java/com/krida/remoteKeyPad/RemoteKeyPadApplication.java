package com.krida.remoteKeyPad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RemoteKeyPadApplication {

	public static void main(String[] args) {
		SpringApplication.run(RemoteKeyPadApplication.class, args);
	}

}
