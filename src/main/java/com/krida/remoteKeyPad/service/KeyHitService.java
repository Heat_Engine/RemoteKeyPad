package com.krida.remoteKeyPad.service;

public interface KeyHitService {
	String hitKey(String key);
}
