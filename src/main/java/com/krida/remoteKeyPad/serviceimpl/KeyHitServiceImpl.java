package com.krida.remoteKeyPad.serviceimpl;

import java.io.IOException;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.krida.remoteKeyPad.service.KeyHitService;

@Service
public class KeyHitServiceImpl implements KeyHitService {
	private final ProcessBuilder builder;
	private final Environment env;
	public KeyHitServiceImpl(Environment environment) {
		this.env = environment;
		this.builder = new ProcessBuilder();
	}

	@Override
	public String hitKey(String key) {
		// TODO Auto-generated method stub
		String command = "$wshell = New-Object -ComObject wscript.shell;$wshell.SendKeys('?')".replace("?", env.getProperty(key));
		builder.command("powershell", command);
		try {
			Process process = builder.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return key;
	}

}
